package com.company;

public class Calculator {


    public Calculator() {
    }

    public int addition(int x,int y){

        return x + y;
    }

    public int addition(int[] numArray){
        int sum = 0;
        for(int i : numArray){
            sum = sum + i;
        }
        return sum;
    }

    public int subtract(int x, int y){
        return x-y;
    }

    public int multiplication(int x, int y){
        return x*y;
    }

    public double division(int x, int y){
        return (double)x/y;
    }

}
