package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {

    //Arrange, act, assert
    @Test
    public void calculateInts_addNumbersTogether(){
        //arrange
        int x = 1;
        int y = 2;

        int expected = 3;
        Calculator calculator = new Calculator();

        //act
        int actual = calculator.addition(x,y);
        //assert
        assertEquals(expected,actual);

    }

    @Test
    public void calculateInts_addNumbersInArrayTogether(){
        //arrange
        int[] arrayNum = {1,4,5,6,8};
        int expected = 1+4+5+6+8;
        Calculator calculator = new Calculator();

        //act
        int actual = calculator.addition(arrayNum);

        //assert
        assertEquals(expected,actual);

    }

    @Test
    public void calculateInts_subtractXFromY(){
        //arrange
        int x = 4;
        int y = 6;
        int expected = -2;
        Calculator calculator = new Calculator();

        //act
        int actual = calculator.subtract(x,y);

        //assert
        assertEquals(expected, actual);
    }

    @Test
    public void calculateInts_multiplicateXAndY(){
        int x = 4;
        int y = 5;
        int expected = 20;

        Calculator calculator = new Calculator();

        //act
        int actual = calculator.multiplication(x,y);

        //assert
        assertEquals(expected,actual);
    }

    @Test
    public void calculateInts_divideXAndY(){
        int x = 50;
        int y = 5;
        int expected = 10;
        Calculator calculator = new Calculator();

        //act
        double actual = calculator.division(x,y);

        //assert
        assertEquals(expected,actual);
    }

    @Test
    public void calculateInts_divideXAndYDoubles(){
        int x = 5;
        int y = 2;
        double expected = 2.5;
        Calculator calculator = new Calculator();

        //act
        double actual = calculator.division(x,y);

        //assert
        assertEquals(expected,actual);
    }

}